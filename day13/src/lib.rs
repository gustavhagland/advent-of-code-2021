pub enum Problem {
    First,
    Second,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Point(i32, i32);

#[derive(Debug, PartialEq)]
enum Axis {
    Horizontal,
    Vertical,
}

#[derive(Debug, PartialEq)]
struct FoldInstruction {
    axis: Axis,
    position: i32,
}

fn parse_input(lines: impl Iterator<Item = String>) -> (Vec<Point>, Vec<FoldInstruction>) {
    let mut parsing_points = true;
    let mut points = Vec::new();
    let mut instructions = Vec::new();
    for line in lines {
        if line.chars().all(|c| c.is_ascii_whitespace()) {
            parsing_points = false
        } else if parsing_points {
            let mut split_line = line.split(",");
            points.push(Point(
                split_line
                    .next()
                    .expect("No x coord")
                    .parse()
                    .expect("Could not parse y"),
                split_line
                    .next()
                    .expect("No y coord")
                    .parse()
                    .expect("Could not parse x"),
            ))
        } else {
            let instruction = line.trim_start_matches("fold along ");
            let mut split_instr = instruction.split("=");
            let axis = match split_instr.next().expect("No axis in fold instr") {
                "x" => Axis::Vertical,
                "y" => Axis::Horizontal,
                _ => panic!("Unkown format in string"),
            };
            let position = split_instr
                .next()
                .expect("Could not read fold position")
                .parse()
                .expect("Could not parse fold pos");
            instructions.push(FoldInstruction { axis, position })
        }
    }
    (points, instructions)
}

fn fold_points(points: &Vec<Point>, instruction: &FoldInstruction) -> Vec<Point> {
    let mut folded_points = Vec::new();
    for point in points.iter() {
        match instruction.axis {
            Axis::Horizontal => {
                if point.1 >= instruction.position {
                    let folded = Point(
                        point.0,
                        instruction.position - (point.1 - instruction.position),
                    );
                    if !folded_points.contains(&folded) {
                        folded_points.push(folded)
                    }
                } else {
                    if !folded_points.contains(point) {
                        folded_points.push(*point)
                    }
                }
            }
            Axis::Vertical => {
                if point.0 >= instruction.position {
                    let folded = Point(
                        instruction.position - (point.0 - instruction.position),
                        point.1,
                    );
                    if !folded_points.contains(&folded) {
                        folded_points.push(folded)
                    }
                } else {
                    if !folded_points.contains(point) {
                        folded_points.push(*point)
                    }
                }
            }
        }
    }
    folded_points
}

pub fn run(lines: impl Iterator<Item = String>, mode: Problem) {
    let (points, fold_instructions) = parse_input(lines);
    match mode {
        Problem::First => println!(
            "num: {:?}",
            fold_points(&points, &fold_instructions[0]).len()
        ),
        Problem::Second => {
            let mut folded_points = points;
            for instruction in fold_instructions.iter() {
                folded_points = fold_points(&folded_points, instruction);
            }
            let max_x = folded_points
                .iter()
                .map(|point| point.0)
                .max()
                .expect("No max x?");
            let max_y = folded_points
                .iter()
                .map(|point| point.1)
                .max()
                .expect("No max y?");
            for y in 0..max_y + 1 {
                for x in 0..max_x + 1 {
                    print!(
                        "{}",
                        if folded_points.contains(&Point(x, y)) {
                            "#"
                        } else {
                            "."
                        }
                    )
                }
                println!();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_simple() {
        //GIVEN
        let input = vec!["1,0", "", "fold along y=1"];

        //WHEN
        let (points, fold_instructions): (Vec<Point>, Vec<FoldInstruction>) =
            parse_input(input.iter().map(|line| String::from(*line)));

        //THEN
        assert!(vec![Point(1, 0)]
            .iter()
            .zip(points.iter())
            .all(|(a, b)| a == b));
        assert!(vec![FoldInstruction {
            axis: Axis::Horizontal,
            position: 1
        }]
        .iter()
        .zip(fold_instructions.iter())
        .all(|(a, b)| a == b));
    }

    #[test]
    fn fold_point() {
        // .        #
        // -   ---> -
        // #
        //GIVEN
        let points = vec![Point(0, 2)];
        let instruction = FoldInstruction {
            axis: Axis::Horizontal,
            position: 1,
        };
        //WHEN
        let folded_points = fold_points(&points, &instruction);
        //THEN
        assert_eq!(1, folded_points.len());
        assert_eq!(Point(0, 0), folded_points[0]);
    }

    #[test]
    fn fold_collapse_overlapping() {
        // #        #
        // -   ---> -
        // #
        //GIVEN
        let points = vec![Point(0, 0), Point(0, 2)];
        let instruction = FoldInstruction {
            axis: Axis::Horizontal,
            position: 1,
        };
        //WHEN
        let folded_points = fold_points(&points, &instruction);
        //THEN
        assert_eq!(1, folded_points.len());
        assert_eq!(Point(0, 0), folded_points[0]);
    }
}
