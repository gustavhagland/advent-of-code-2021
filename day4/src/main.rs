use std::env;
use std::io::{self, BufRead};

fn parse_board_line(line: &String) -> [i32; 5] {
    let mut split_line = line.split_whitespace();
    [
        split_line
            .next()
            .expect("Not enough numbers in board line")
            .parse()
            .expect("Could not parse number in board line"),
        split_line
            .next()
            .expect("Not enough numbers in board line")
            .parse()
            .expect("Could not parse number in board line"),
        split_line
            .next()
            .expect("Not enough numbers in board line")
            .parse()
            .expect("Could not parse number in board line"),
        split_line
            .next()
            .expect("Not enough numbers in board line")
            .parse()
            .expect("Could not parse number in board line"),
        split_line
            .next()
            .expect("Not enough numbers in board line")
            .parse()
            .expect("Could not parse number in board line"),
    ]
}

fn parse_boards(lines: Vec<String>) -> Vec<[[i32; 5]; 5]> {
    let mut line_iter = lines.iter().peekable();
    let mut boards: Vec<[[i32; 5]; 5]> = Vec::new();
    while !line_iter.peek().is_none() {
        line_iter.next(); //always an empty line first
        boards.push([
            parse_board_line(line_iter.next().expect("")),
            parse_board_line(line_iter.next().expect("")),
            parse_board_line(line_iter.next().expect("")),
            parse_board_line(line_iter.next().expect("")),
            parse_board_line(line_iter.next().expect("")),
        ])
    }
    return boards;
}

fn parse_puzzle_input() -> (Vec<i32>, Vec<[[i32; 5]; 5]>) {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let numbers: Vec<i32> = lines
        .next()
        .expect("Could not read first line")
        .expect("Really couldn't read it")
        .split(",")
        .map(|str_num| {
            str_num
                .parse()
                .expect("Could not parse number in first line")
        })
        .collect();
    let boards: Vec<[[i32; 5]; 5]> = parse_boards(
        lines
            .map(|line| line.expect("Could not read line"))
            .collect(),
    );
    (numbers, boards)
}

fn any_column_matches(board: &[[i32; 5]; 5], drawn_numbers: &Vec<i32>) -> bool {
    for x in 0..5 {
        let mut all_matching = true;
        for y in 0..5 {
            if !drawn_numbers.contains(&board[y][x]) {
                all_matching = false;
            }
        }
        if all_matching {
            return true;
        }
    }
    false
}

fn any_row_matches(board: &[[i32; 5]; 5], drawn_numbers: &Vec<i32>) -> bool {
    board
        .iter()
        .any(|row| row.iter().all(|number| drawn_numbers.contains(number)))
}

fn board_matches(board: &[[i32; 5]; 5], drawn_numbers: &Vec<i32>) -> bool {
    any_row_matches(board, drawn_numbers) || any_column_matches(board, drawn_numbers)
}

fn get_matching_boards(boards: &Vec<[[i32; 5]; 5]>, drawn_numbers: &Vec<i32>) -> Vec<usize> {
    let mut matching_boards = Vec::new();
    for (i, board) in boards.iter().enumerate() {
        if board_matches(board, drawn_numbers) {
            matching_boards.push(i);
        }
    }
    return matching_boards;
}

fn calc_board_score(board: &[[i32; 5]; 5], drawn_numbers: &Vec<i32>, last_num: i32) -> i32 {
    let mut score = 0;
    for row in board {
        for number in row {
            if !drawn_numbers.contains(number) {
                score += number;
            }
        }
    }
    score * last_num
}

fn get_winning_board_score(boards: &Vec<[[i32; 5]; 5]>, numbers: &Vec<i32>) -> i32 {
    let mut drawn_numbers: Vec<i32> = Vec::new();
    for number in numbers {
        drawn_numbers.push(*number);
        let matching_boards = get_matching_boards(&boards, &drawn_numbers);
        if matching_boards.len() > 0 {
            return matching_boards
                .iter()
                .map(|board| calc_board_score(&boards[*board], &drawn_numbers, *number))
                .max()
                .expect("Something went wrong");
        }
    }
    return -1;
}

fn problem1() {
    let (numbers, boards) = parse_puzzle_input();
    let winning_score = get_winning_board_score(&boards, &numbers);
    println!("{}", winning_score);
}

fn get_losing_board_score(boards: &mut Vec<[[i32; 5]; 5]>, numbers: &Vec<i32>) -> i32 {
    let mut drawn_numbers: Vec<i32> = Vec::new();
    for number in numbers {
        drawn_numbers.push(*number);
        let matching_boards = get_matching_boards(&boards, &drawn_numbers);
        if matching_boards.len() > 0 {
            if boards.len() > 1 {
                for index in matching_boards.iter().rev() {
                    boards.remove(*index);
                }
            } else {
                return calc_board_score(
                    &boards[*matching_boards
                        .first()
                        .expect("Matching board is empty when it shouldn't be")],
                    &drawn_numbers,
                    *number,
                );
            }
        }
    }
    return -1;
}

fn problem2() {
    let (numbers, boards) = parse_puzzle_input();
    let losing_score = get_losing_board_score(&mut boards.to_vec(), &numbers);
    println!("{}", losing_score);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mode: &String = &args[1];
    match mode.as_str() {
        "1" => problem1(),
        "2" => problem2(),
        _ => println!("du e full"),
    }
}
