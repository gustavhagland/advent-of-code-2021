fn get_fuel_cost(crab_pos: i32, target_pos: i32) -> i32 {
    let diff = (crab_pos - target_pos).abs();
    return diff * (diff + 1) / 2;
}

fn main() {
    let mut input_line = String::new();
    std::io::stdin()
        .read_line(&mut input_line)
        .expect("Could not read line");
    let crabs: Vec<i32> = input_line
        .split(",")
        .map(|str_crab| str_crab.parse().expect("Could not parse crab"))
        .collect();
    let max_pos: i32 = *crabs.iter().max().expect("Could not get maximum crab");
    let mut costs: Vec<i32> = vec![0; max_pos as usize];
    for crab in crabs {
        for cost_i in 0..costs.len() {
            costs[cost_i] += get_fuel_cost(crab, cost_i as i32)
        }
    }
    println!("{}", costs.iter().min().expect("Could not get min"))
}
