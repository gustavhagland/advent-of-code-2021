use std::collections::HashMap;
use std::io::BufRead;

#[derive(Debug)]
struct Entry {
    patterns: Vec<String>,
    output: Vec<String>,
}

fn parse_line(line: String) -> Entry {
    let mut split_line = line.split("|");
    let patterns = split_line
        .next()
        .expect("Patterns missing")
        .split_whitespace()
        .map(|elem| String::from(elem))
        .collect();
    let output = split_line
        .next()
        .expect("Output values missing")
        .split_whitespace()
        .map(|elem| String::from(elem))
        .collect();
    Entry { patterns, output }
}

fn get_inputs() -> Vec<Entry> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|line| parse_line(line.expect("Could not read line")))
        .collect()
}

fn is_unique(output: &String) -> bool {
    output.len() == 2 || output.len() == 3 || output.len() == 4 || output.len() == 7
}

fn get_combinations(items: Vec<char>) -> Vec<Vec<char>> {
    if items.len() == 1 {
        return vec![items];
    }
    let mut copy = Vec::new();
    let mut iter = items.iter();
    let new_elem = iter.next().expect("oopsie");
    for c in iter {
        copy.push(*c)
    }
    let next_combinations = get_combinations(copy);
    let mut combinations = Vec::new();
    for combination in next_combinations {
        for i in 0..combination.len() + 1 {
            let mut new_combination: Vec<char> = combination.iter().map(|c| *c).collect();
            new_combination.insert(i, *new_elem);
            combinations.push(new_combination);
        }
    }
    combinations
}

fn get_map(chars: &[char; 7], combination: &Vec<char>) -> HashMap<char, char> {
    let mut map = HashMap::new();
    for (input, output) in combination.iter().zip(chars.iter()) {
        map.insert(*input, *output);
    }
    return map;
}

fn pattern_match(pattern: &str, number: &str) -> bool {
    pattern.len() == number.len() && pattern.chars().all(|c| number.contains(c))
}

fn is_valid(input: &String, map: &HashMap<char, char>, numbers: &[&str; 10]) -> bool {
    let mapped: String = input.chars().map(|c| map[&c]).collect();
    numbers
        .iter()
        .any(|number| pattern_match(number, mapped.as_str()))
}

fn calc_solution(
    possibility: &HashMap<char, char>,
    outputs: &Vec<String>,
    numbers: &[&str; 10],
) -> u32 {
    let mut result = 0;
    for (i, output) in outputs.iter().enumerate() {
        let mapped: String = output.chars().map(|c| possibility[&c]).collect();
        let digit = numbers
            .iter()
            .position(|num| pattern_match(num, mapped.as_str()))
            .expect("nani!?");
        result += digit as u32 * 10u32.pow(3 - i as u32);
    }
    return result;
}

fn main() {
    let entries = get_inputs();
    println!(
        "Num unique: {}",
        entries
            .iter()
            .flat_map(|entry| entry.output.iter())
            .filter(|output| is_unique(output))
            .count()
    );

    let chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
    let combinations = get_combinations(chars.to_vec());
    //Gå igenom varje entry
    //Filtrera ut alla possibilities där entries inte matcher en shape
    //Stanna när det bara är en kvar
    let numbers = [
        "abcefg", "cf", "acdeg", "acdgf", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg",
    ];
    let mut result = 0;
    for entry in entries.iter() {
        let mut possibilities: Vec<HashMap<char, char>> = combinations
            .iter()
            .map(|comb| get_map(&chars, comb))
            .collect();
        for pattern in entry.patterns.iter() {
            possibilities.retain(|possibility| is_valid(pattern, possibility, &numbers));
            if possibilities.len() == 1 {
                result += calc_solution(&possibilities[0], &entry.output, &numbers);
                break;
            }
        }
    }
    println!("result is {}", result)
}

#[cfg(test)]
mod test {
    #[test]
    fn test_is_valid() {
        let numbers = [
            "abcefg", "cf", "acdeg", "acdgf", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg",
        ];
        let chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        let map = super::get_map(&chars, &vec!['d', 'e', 'a', 'f', 'g', 'b', 'c']);
        let false_map = super::get_map(&chars, &vec!['f', 'c', 'a', 'g', 'e', 'd', 'b']);
        let pattern = "dab";
        assert!(super::is_valid(&String::from(pattern), &map, &numbers));
        assert!(!super::is_valid(
            &String::from(pattern),
            &false_map,
            &numbers
        ));
    }
}
