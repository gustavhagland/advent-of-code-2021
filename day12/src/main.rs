use day12::{run, Problem};
use std::fs::File;
use std::io::{self, BufRead};
fn main() {
    let args: Vec<String> = std::env::args().collect();
    let (file_path, run_mode) = parse_args(&args).expect("Problem parsing args");
    let file = File::open(file_path).expect("Could not open file: ");
    run(
        io::BufReader::new(file)
            .lines()
            .map(|line| line.expect("Could not read line")),
        run_mode,
    )
}

fn parse_args(args: &[String]) -> Result<(String, Problem), &str> {
    if args.len() < 3 {
        return Err("Not enough arguments");
    }
    let file = args[1].clone();
    match args[2].as_str() {
        "1" => Ok((file, Problem::First)),
        "2" => Ok((file, Problem::Second)),
        _ => Err("Unknown run mode"),
    }
}
