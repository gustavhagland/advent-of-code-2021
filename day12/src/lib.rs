use std::collections::HashMap;
pub enum Problem {
    First,
    Second,
}

#[derive(Debug)]
struct NodeMap {
    nodes: HashMap<String, Vec<String>>,
}

impl NodeMap {
    pub fn from_iter(iter: impl Iterator<Item = String>) -> NodeMap {
        let mut nodes: HashMap<String, Vec<String>> = HashMap::new();
        for str_edge in iter {
            let mut split = str_edge.split("-");
            let edge_start = String::from(split.next().expect("Could not get start of edge"));
            let edge_end = String::from(split.next().expect("Could not get end of egde"));
            if !nodes.contains_key(&edge_start) {
                nodes.insert(edge_start.clone(), vec![edge_end.clone()]);
            } else {
                nodes
                    .get_mut(&edge_start)
                    .expect("Key not found")
                    .push(edge_end.clone());
            }
            if !nodes.contains_key(&edge_end) {
                nodes.insert(edge_end.clone(), vec![edge_start.clone()]);
            } else {
                nodes
                    .get_mut(&edge_end)
                    .expect("Key not found")
                    .push(edge_start);
            }
        }
        NodeMap { nodes }
    }

    fn get_paths_with_visited_checker<F>(&self, checker: F) -> usize
    where
        F: Fn(&Vec<String>, &String) -> bool,
    {
        let mut paths: Vec<Vec<String>> = vec![vec![String::from("start")]];
        let mut complete_paths = vec![];
        while let Some(path) = paths.pop() {
            if path.contains(&String::from("end")) {
                complete_paths.push(path);
                continue;
            }
            for node in self.nodes[&path[path.len() - 1]]
                .iter()
                .filter(|node| checker(&path, node))
            {
                let mut new_path = path.clone();
                new_path.push(node.clone());
                paths.insert(0, new_path);
            }
        }
        for path in complete_paths.iter() {
            for node in path {
                print!("{}-", node);
            }
            println!();
        }
        complete_paths.len()
    }

    pub fn get_num_paths(&self) -> usize {
        self.get_paths_with_visited_checker(|path, node| {
            node.chars().all(|c| c.is_ascii_uppercase()) || !path.contains(node)
        })
    }

    fn is_allowed_to_visit(path: &Vec<String>, node: &String) -> bool {
        if node.chars().all(|c| c.is_ascii_uppercase()) {
            return true;
        }
        if node == "start" {
            return false;
        }
        if node == "end" {
            return !path.contains(node);
        }
        //all nodes here are normal lowercase
        let mut counter: HashMap<&String, u8> = HashMap::new();
        let mut has_small_visited_twice = false;
        for visited in path
            .iter()
            .filter(|p_node| p_node.chars().all(|c| c.is_ascii_lowercase()))
        {
            if counter.contains_key(visited) {
                has_small_visited_twice = true;
                break;
            } else {
                counter.insert(visited, 0);
            }
        }
        if has_small_visited_twice {
            return !path.contains(node);
        } else {
            path.iter().filter(|visited| *visited == node).count() < 2
        }
    }

    pub fn get_num_paths_full(&self) -> usize {
        self.get_paths_with_visited_checker(NodeMap::is_allowed_to_visit)
    }
}

pub fn run(lines: impl Iterator<Item = String>, mode: Problem) {
    let map = NodeMap::from_iter(lines);
    println!("{:?}", map);
    match mode {
        Problem::First => println!("Num paths: {}", map.get_num_paths()),
        Problem::Second => println!("Num paths: {}", map.get_num_paths_full()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_all_paths() {
        //GIVEN
        let cases = vec![
            (1, vec!["start-end"]),
            (4, vec!["start-a", "start-b", "a-b", "a-end", "b-end"]),
            (
                10,
                vec!["start-A", "start-b", "A-c", "A-b", "b-d", "A-end", "b-end"],
            ),
        ];
        for (num_paths, paths) in cases {
            let map = NodeMap::from_iter(paths.iter().map(|path| String::from(*path)));
            assert_eq!(num_paths, map.get_num_paths())
        }
    }

    #[test]
    fn test_get_all_paths_full_simple() {
        //GIVEN
        //*-a-#, *-b-#, *-a-b-#, *-b-a-#, *-a-b-a-#, *-b-a-b-#
        test_get_num_paths_full(6, vec!["start-a", "start-b", "a-b", "a-end", "b-end"])
    }

    #[test]
    fn test_full_extra_node() {
        test_get_num_paths_full(
            9,
            vec!["start-a", "start-b", "a-b", "a-end", "b-end", "b-c"],
        )
    }

    #[test]
    fn test_full_small_variant() {
        test_get_num_paths_full(
            36,
            vec!["start-A", "start-b", "A-c", "A-b", "b-d", "A-end", "b-end"],
        )
    }

    fn test_get_num_paths_full(num_paths: usize, path: Vec<&str>) {
        let map = NodeMap::from_iter(path.iter().map(|str_path| String::from(*str_path)));
        assert_eq!(num_paths, map.get_num_paths_full())
    }
}
