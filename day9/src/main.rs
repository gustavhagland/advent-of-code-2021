use std::io::BufRead;
mod heightmap {
    pub struct HeightMap {
        height_points: Vec<Vec<u8>>,
        pub height: usize,
        pub width: usize,
    }

    impl HeightMap {
        pub fn from_str_lines(lines: impl Iterator<Item = String>) -> HeightMap {
            let mut points: Vec<Vec<u8>> = lines
                .map(|line| {
                    line.chars()
                        .map(|c| c.to_digit(10).expect("Could not parse char") as u8)
                        .collect()
                })
                .collect();
            for row in points.iter_mut() {
                row.insert(0, 10);
                row.push(10)
            }
            points.insert(
                0,
                vec![10; points.first().expect("No row at first pos").len()],
            );
            points.insert(
                points.len(),
                vec![10; points.first().expect("No row at first pos").len()],
            );
            HeightMap {
                height: points.len() - 2,
                width: points.first().expect("What?!").len() - 2,
                height_points: points,
            }
        }

        pub fn get_point(&self, x: i32, y: i32) -> u8 {
            self.height_points[(y + 1) as usize][(x + 1) as usize]
        }

        fn get_neighbours(&self, x: i32, y: i32) -> Vec<(i32, i32)> {
            vec![(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
        }

        pub fn is_low_point(&self, x: i32, y: i32) -> bool {
            let point = self.get_point(x, y);
            self.get_neighbours(x, y)
                .iter()
                .all(|(x, y)| self.get_point(*x, *y) > point)
        }

        fn get_basin_neighbours(
            &self,
            visited: &Vec<(i32, i32)>,
            x: i32,
            y: i32,
        ) -> Vec<(i32, i32)> {
            self.get_neighbours(x, y)
                .iter()
                .filter(|(x, y)| self.get_point(*x, *y) < 9 && !visited.contains(&(*x, *y)))
                .map(|item| *item)
                .collect()
        }

        pub fn get_basin_size(&self, x: i32, y: i32) -> u32 {
            let mut visited: Vec<(i32, i32)> = Vec::new();
            let mut active: Vec<(i32, i32)> = Vec::new();
            active.insert(0, (x, y));
            visited.push((x, y));
            while active.len() > 0 {
                let (x, y) = active.pop().expect("No items left?");
                let neighbours = self.get_basin_neighbours(&visited, x, y);
                for neighbour in neighbours {
                    active.insert(0, neighbour);
                    visited.push(neighbour);
                }
            }
            visited.len() as u32
        }
    }
}

fn main() {
    let map = heightmap::HeightMap::from_str_lines(
        std::io::stdin()
            .lock()
            .lines()
            .map(|line| line.expect("Could not read line")),
    );
    let mut low_points: Vec<(i32, i32)> = Vec::new();
    for y in 0..map.height {
        for x in 0..map.width {
            if map.is_low_point(x as i32, y as i32) {
                low_points.push((x as i32, y as i32));
            }
        }
    }
    println!(
        "Sum low points: {:?}",
        low_points
            .iter()
            .map(|(x, y)| map.get_point(*x, *y) as u32 + 1)
            .sum::<u32>()
    );
    let mut basin_sizes: Vec<u32> = low_points
        .iter()
        .map(|(x, y)| map.get_basin_size(*x, *y))
        .collect();
    basin_sizes.sort_unstable_by(|a, b| a.cmp(b).reverse());
    println!("{}", basin_sizes[0] * basin_sizes[1] * basin_sizes[2])
}
