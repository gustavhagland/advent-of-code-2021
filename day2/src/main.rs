use std::env;
use std::io::{self, BufRead};

fn problem1() {
    let mut depth = 0;
    let mut horizontal = 0;
    for line in io::stdin().lock().lines() {
        let line = line.expect("Could not read :(");
        let mut split = line.split(" ");
        let command = split.next().expect("No value left?");
        let value: u32 = split
            .next()
            .expect("No value left?")
            .parse()
            .expect("No integer?");
        if command == "forward" {
            horizontal += value;
        } else if command == "up" {
            depth -= value;
        } else {
            depth += value;
        }
    }
    println!("{}", depth * horizontal);
}

fn problem2() {
    let mut aim = 0;
    let mut depth = 0;
    let mut horizontal = 0;
    for line in io::stdin().lock().lines() {
        let line = line.expect("Could not read line");
        let mut split = line.split(" ");
        let command = split.next().expect("Could not get command");
        let value: i32 = split
            .next()
            .expect("Could not get value")
            .parse()
            .expect("Could not parse int");
        match command {
            "down" => aim += value,
            "up" => aim -= value,
            "forward" => {
                horizontal += value;
                depth += aim * value;
            }
            _ => println!("Unexpected command"),
        }
    }
    println!("{}", horizontal * depth);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mode = &args[1];
    match mode.as_str() {
        "1" => problem1(),
        "2" => problem2(),
        _ => println!("uhm que pasa?"),
    }
}
