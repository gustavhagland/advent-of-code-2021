use std::env;
use std::io;

fn get_grid(days: usize) -> Vec<[i64; 9]> {
    let mut grid: Vec<[i64; 9]> = Vec::new();
    grid.push([1; 9]);
    for current_day in 1..days + 1 {
        let mut new_vals = [0; 9];
        new_vals[0] = grid[current_day - 1][6] + grid[current_day - 1][8];
        for current_fish in 1..new_vals.len() {
            new_vals[current_fish] = grid[current_day - 1][current_fish - 1];
        }
        grid.push(new_vals);
    }
    grid
}

fn main() {
    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .expect("Could not read line");
    let given_fishes: Vec<i32> = line
        .split(",")
        .map(|str_fish| str_fish.parse().expect("Could not parse fish"))
        .collect();
    let days: usize = env::args()
        .nth(1)
        .expect("No numdays arg found")
        .parse()
        .expect("Numdays args must be an int");
    let grid = get_grid(days);
    println!(
        "{}",
        given_fishes
            .iter()
            .map(|fish| grid.last().expect("Empty grid")[*fish as usize])
            .sum::<i64>()
    );
}
