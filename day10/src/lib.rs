use std::collections::HashMap;

struct ChunkResult {
    syntax_error: Option<char>,
    completion: Vec<char>,
    line_rest: String,
}
fn is_chunk_end(c: &char, chunk_map: &HashMap<char, char>) -> bool {
    chunk_map.values().find(|val| *val == c).is_some()
}

fn validate_line(line: String, chunk_map: &HashMap<char, char>) -> ChunkResult {
    let mut completions = Vec::new();
    let mut result = validate_block(line, chunk_map);
    completions.extend(result.completion.iter());
    if result.syntax_error.is_some() {
        return result;
    }
    while result.line_rest.len() > 0 {
        result = validate_block(result.line_rest, chunk_map);
        completions.extend(result.completion.iter());
        if result.syntax_error.is_some() {
            return result;
        }
    }
    ChunkResult {
        syntax_error: None,
        completion: completions,
        line_rest: String::from(""),
    }
}

//Does not handle empty strings
fn validate_block(line: String, chunk_map: &HashMap<char, char>) -> ChunkResult {
    let first_char = line.chars().nth(0).expect("No first char");
    //if it's the end of the string
    if line.len() == 1 {
        if is_chunk_end(&first_char, chunk_map) {
            return ChunkResult {
                syntax_error: Some(first_char),
                completion: vec![],
                line_rest: String::from(""),
            };
        } else {
            return ChunkResult {
                syntax_error: None,
                completion: vec![chunk_map[&first_char]],
                line_rest: String::from(""),
            };
        }
    }
    //if it is a syntax error
    if is_chunk_end(&first_char, chunk_map) {
        return ChunkResult {
            syntax_error: Some(first_char),
            completion: vec![],
            line_rest: line.chars().skip(1).collect(),
        };
    }
    //If it completes the chunk
    if line.chars().nth(1).expect("No first char") == chunk_map[&first_char] {
        //can just validate the rest here?
        return ChunkResult {
            syntax_error: None,
            completion: vec![],
            line_rest: line.chars().skip(2).collect(),
        };
    }
    //else validate the subchunk and go again
    let sub_chunk_result = validate_block(line.chars().skip(1).collect(), chunk_map);
    if sub_chunk_result.syntax_error.is_some() {
        return sub_chunk_result;
    }
    let next_line = String::from(first_char)
        .chars()
        .chain(sub_chunk_result.line_rest.chars())
        .collect();
    let next_result = validate_block(next_line, chunk_map);
    ChunkResult {
        syntax_error: next_result.syntax_error,
        completion: sub_chunk_result
            .completion
            .iter()
            .chain(next_result.completion.iter())
            .map(|c| *c)
            .collect(),
        line_rest: next_result.line_rest,
    }
}

fn get_completion_score(completion: &Vec<char>) -> u64 {
    let mut score = 0;
    let score_map = HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4)]);
    for c in completion.iter() {
        score *= 5;
        score += score_map[c];
    }
    score
}

pub fn run(lines: impl Iterator<Item = String>) {
    let mut syntax_score: u32 = 0;
    let score_table: std::collections::HashMap<char, u16> =
        std::collections::HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
    let chunk_map: HashMap<char, char> =
        HashMap::from([('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')]);
    let mut completion_scores = Vec::new();
    for line in lines {
        let result = validate_line(line.clone(), &chunk_map);
        if let Some(syntax_error) = result.syntax_error {
            syntax_score += score_table[&syntax_error] as u32;
        } else {
            completion_scores.push(get_completion_score(&result.completion))
        }
    }
    println!("score: {}", syntax_score);
    completion_scores.sort_unstable();
    println!("{:?}", completion_scores[completion_scores.len() / 2])
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_get_completion() {
        //GIVEN
        let cases: Vec<(&str, &str)> = vec![
            ("{", "}"),
            ("{<>", "}"),
            ("<>", ""),
            ("{{", "}}"),
            ("{<<>>(", ")}"),
        ];
        let chunk_map = HashMap::from([('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')]);
        //WHEN,THEN
        for (line, expected) in cases {
            let result = validate_line(String::from(line), &chunk_map);
            assert_eq!(expected, String::from_iter(result.completion.iter()))
        }
    }
    #[test]
    fn test_completion_score() {
        assert_eq!(294, get_completion_score(&vec![']', ')', '}', '>']))
    }
}
