use std::io::{self, BufRead};

#[derive(Debug, Copy, Clone)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    /// Parses a string with the format {num},{num} to a point
    fn parse(string: &str) -> Point {
        let mut split_string = string.split(",");
        let x = split_string
            .next()
            .expect("String is in wrong format, could not parse point")
            .parse()
            .expect("Could not parse number, to point");
        let y = split_string
            .next()
            .expect("String is in wrong format, could not parse point")
            .parse()
            .expect("Could not parse number, to point");
        Point { x: x, y: y }
    }
}

#[derive(Debug, Copy, Clone)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn parse(string: &String) -> Line {
        let mut split_string = string.split_whitespace();
        let start = Point::parse(
            split_string
                .next()
                .expect("String is in worng format when parsing line"),
        );
        assert_eq!("->", split_string.next().expect("No arrow here"));
        let end = Point::parse(
            split_string
                .next()
                .expect("String is in worng format when parsing line"),
        );
        Line {
            start: start,
            end: end,
        }
    }

    fn is_vertical(&self) -> bool {
        self.start.y == self.end.y
    }

    fn is_horizontal(&self) -> bool {
        self.start.x == self.end.x
    }
}

/// Paints he provided line onto the grid
fn paint_line(line: &Line, grid: &mut Vec<Vec<u32>>) {
    if line.is_vertical() {
        for x in line.start.x.min(line.end.x)..line.start.x.max(line.end.x) + 1 {
            grid[line.start.y][x] += 1;
        }
    } else if line.is_horizontal() {
        for y in line.start.y.min(line.end.y)..line.start.y.max(line.end.y) + 1 {
            grid[y][line.start.x] += 1;
        }
    } else {
        if line.start.x <= line.end.x && line.start.y <= line.end.y {
            for i in 0..line.end.x - line.start.x + 1 {
                grid[line.start.y + i][line.start.x + i] += 1;
            }
        } else if line.start.x >= line.end.x && line.start.y >= line.end.y {
            for i in 0..line.start.x - line.end.x + 1 {
                grid[line.end.y + i][line.end.x + i] += 1;
            }
        } else if line.start.x >= line.end.x {
            for i in 0..line.start.x - line.end.x + 1 {
                grid[line.start.y + i][line.start.x - i] += 1;
            }
        } else {
            for i in 0..line.end.x - line.start.x + 1 {
                grid[line.start.y - i][line.start.x + i] += 1;
            }
        }
    }
}

fn get_num_overlapping_points(lines: Vec<Line>) -> u32 {
    let max_x = lines.iter().fold(0, |max, current| {
        let current_max = current.start.x.max(current.end.x);
        if current_max > max {
            current_max
        } else {
            max
        }
    });
    let max_y = lines.iter().fold(0, |max, current| {
        let current_max = current.start.y.max(current.end.y);
        if current_max > max {
            current_max
        } else {
            max
        }
    });
    let mut grid: Vec<Vec<u32>> = vec![vec![0; max_x + 1]; max_y + 1];
    for line in lines {
        paint_line(&line, &mut grid)
    }
    grid.iter()
        .flat_map(|row| row.iter())
        .fold(0, |acc, num| if *num > 1 { acc + 1 } else { acc })
}

fn main() {
    let lines: Vec<Line> = io::stdin()
        .lock()
        .lines()
        .map(|lineres| lineres.expect("Could not read line"))
        .map(|str_line| Line::parse(&str_line))
        .collect();
    let mut straight_lines = lines.to_vec();
    straight_lines.retain(|line| line.is_vertical() || line.is_horizontal());

    println!(
        "Num points vertical and horizontal: {}",
        get_num_overlapping_points(straight_lines)
    );
    println!(
        "All overlapping points: {}",
        get_num_overlapping_points(lines)
    );
}
