#[derive(Debug, Copy, Clone)]
struct Octopus {
    energy: u8,
    has_flashed: bool,
}

impl Octopus {
    pub fn from(c: char) -> Octopus {
        Octopus {
            energy: c.to_digit(10).expect("Could not parse energy") as u8,
            has_flashed: false,
        }
    }

    pub fn increment(&mut self) {
        if !self.has_flashed {
            self.energy += 1;
        }
    }

    pub fn get_energy(&self) -> u8 {
        self.energy
    }

    pub fn reset_flash(&mut self) {
        self.has_flashed = false;
    }

    pub fn flash(&mut self) {
        if self.energy < 10 {
            panic!("Invalid flash!!!")
        }
        self.energy = 0;
        self.has_flashed = true;
    }
}

#[derive(Debug)]
struct OctopusField {
    octopie: Vec<Vec<Octopus>>,
    num_flashed: u32,
}

impl OctopusField {
    pub fn from_iter(iter: impl Iterator<Item = String>) -> OctopusField {
        OctopusField {
            octopie: iter
                .map(|line| line.chars().map(|c| Octopus::from(c)).collect())
                .collect(),
            num_flashed: 0,
        }
    }

    fn is_valid_pos(&self, x: usize, y: usize) -> bool {
        y < self.octopie.len()
            && x < self
                .octopie
                .first()
                .expect("Cannot check pos on empty field")
                .len()
    }

    fn get_neighbours_pos(&self, x: usize, y: usize) -> Vec<(usize, usize)> {
        let mut neighbours = vec![];
        for y_delta in 0..3 {
            for x_delta in 0..3 {
                if !(x_delta == 1 && y_delta == 1)
                    && x + x_delta > 0
                    && y + y_delta > 0
                    && self.is_valid_pos(x + x_delta - 1, y + y_delta - 1)
                {
                    neighbours.push((x + x_delta - 1, y + y_delta - 1))
                }
            }
        }
        neighbours
    }

    fn simulate_flashes_step(&mut self) -> bool {
        let mut flashed_octopie_pos = Vec::new();
        for (y, row) in self.octopie.iter_mut().enumerate() {
            for (x, octopie) in row.iter_mut().enumerate() {
                if octopie.get_energy() > 9 {
                    octopie.flash();
                    self.num_flashed += 1;
                    flashed_octopie_pos.push((x, y));
                }
            }
        }
        for (x, y) in flashed_octopie_pos.iter() {
            for (n_x, n_y) in self.get_neighbours_pos(*x, *y) {
                self.octopie[n_y][n_x].increment()
            }
        }
        flashed_octopie_pos.len() > 0
    }

    pub fn simulate_step(&mut self) {
        for row in self.octopie.iter_mut() {
            for octopus in row.iter_mut() {
                octopus.reset_flash();
                octopus.increment();
            }
        }
        while self.simulate_flashes_step() {}
    }
    pub fn print(&self) {
        for row in self.octopie.iter() {
            for octopus in row.iter() {
                print!("{} ", octopus.get_energy())
            }
            println!()
        }
    }
}

pub fn run(lines: impl Iterator<Item = String>) {
    let mut field = OctopusField::from_iter(lines);
    let mut i = 0;
    loop {
        let last_val = field.num_flashed;
        field.simulate_step();
        if field.num_flashed - last_val == 100 {
            println!("at {}", i);
            break;
        }
        if i == 99 {
            println!("{}", field.num_flashed);
        }
        i += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_flashes() {
        let cases = vec![
            (1, vec!["19", "11"]),
            (2, vec!["89", "66"]),
            (4, vec!["89", "67"]),
        ];
        for (expected, str_input) in cases {
            let mut field =
                OctopusField::from_iter(str_input.iter().map(|line| String::from(*line)));
            field.simulate_step();
            assert_eq!(expected, field.num_flashed);
        }
    }
}
