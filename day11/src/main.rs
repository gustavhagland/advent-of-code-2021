use day11::run;
use std::fs::File;
use std::io::{self, BufRead};
fn main() {
    let args: Vec<String> = std::env::args().collect();
    let file_path = parse_args(&args).expect("Problem parsing args");
    let file = File::open(file_path).expect("Could not open file: ");
    run(io::BufReader::new(file)
        .lines()
        .map(|line| line.expect("Could not read line")))
}

fn parse_args(args: &[String]) -> Result<String, &str> {
    if args.len() < 2 {
        return Err("Not enough arguments");
    }
    Ok(args[1].clone())
}
