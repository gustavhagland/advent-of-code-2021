use std::collections::{HashSet, VecDeque};
use std::fmt::Display;

pub enum Problem {
    First,
    Second,
}

struct CritonMap {
    criton_risk: Vec<Vec<u8>>,
    height: usize,
    width: usize,
}

impl CritonMap {
    pub fn from_iter(lines: impl Iterator<Item = String>) -> CritonMap {
        let criton_risk: Vec<Vec<u8>> = lines
            .map(|line| {
                line.chars()
                    .map(|c| c.to_digit(10).expect("Could not parse risk") as u8)
                    .collect::<Vec<u8>>()
            })
            .collect();
        let height = criton_risk.len();
        let width = criton_risk[0].len();
        CritonMap {
            criton_risk,
            height,
            width,
        }
    }

    pub fn from_iter_extreme(lines: impl Iterator<Item = String>) -> CritonMap {
        let criton_risk: Vec<Vec<u8>> = lines
            .map(|line| {
                line.chars()
                    .map(|c| c.to_digit(10).expect("Could not parse risk") as u8)
                    .collect::<Vec<u8>>()
            })
            .collect();
        let height = criton_risk.len();
        let width = criton_risk[0].len();
        let mut extreme_map: Vec<Vec<u8>> = vec![];
        for y in 0..height * 5 {
            let mut row = vec![];
            for x in 0..width * 5 {
                row.push(
                    ((criton_risk[y % height][x % width] + ((x / width) + (y / height)) as u8 - 1)
                        % 9)
                        + 1,
                )
            }
            extreme_map.push(row);
        }
        let extreme_height = extreme_map.len();
        let extreme_width = extreme_map[0].len();
        CritonMap {
            criton_risk: extreme_map,
            height: extreme_height,
            width: extreme_width,
        }
    }

    fn get_neighbours(&self, node: (usize, usize)) -> Vec<(usize, usize)> {
        let mut neighbours = vec![];
        let (x, y) = node;
        if x > 0 {
            neighbours.push((x - 1, y))
        }
        if x < self.width - 1 {
            neighbours.push((x + 1, y));
        }
        if y > 0 {
            neighbours.push((x, y - 1))
        }
        if y < self.height - 1 {
            neighbours.push((x, y + 1));
        }
        neighbours
    }

    pub fn find_shortest_path(
        &self,
        start: (usize, usize),
        end: (usize, usize),
    ) -> Result<usize, &str> {
        let mut visited = HashSet::from([start]);
        let mut paths: VecDeque<(usize, (usize, usize))> = VecDeque::from([(0, start)]);
        while paths.len() > 0 {
            let (cost, current_node) = paths.pop_front().expect("Somethigns fishy");
            if current_node == end {
                return Ok(cost);
            }
            for neighbour in self.get_neighbours(current_node).iter() {
                if !visited.contains(neighbour) {
                    let new_elem = (
                        cost + self.criton_risk[neighbour.1][neighbour.0] as usize,
                        *neighbour,
                    );
                    match paths.binary_search_by_key(&new_elem.0, |(cost, _path)| *cost) {
                        Ok(index) => paths.insert(index, new_elem),
                        Err(index) => paths.insert(index, new_elem),
                    }
                    visited.insert(*neighbour);
                }
            }
        }
        Err("No path found")
    }
}
impl Display for CritonMap {
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        println!("w: {}, h: {}", self.width, self.height);
        Ok(for row in self.criton_risk.iter() {
            for risk in row.iter() {
                print!("{}", risk);
            }
            println!();
        })
    }
}

pub fn run(lines: impl Iterator<Item = String>, mode: Problem) {
    let map = match mode {
        Problem::First => CritonMap::from_iter(lines),
        Problem::Second => CritonMap::from_iter_extreme(lines),
    };
    let cost = map
        .find_shortest_path((0, 0), (map.width - 1, map.height - 1))
        .expect("Oops");
    println!(" {}", cost);
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn parse_map() {
        //GIVEN
        let input = vec!["12", "34"];
        //When
        let map = CritonMap::from_iter(input.iter().map(|line| String::from(*line)));
        //Then
        assert_eq!(vec![vec![1, 2], vec![3, 4]], map.criton_risk);
        assert_eq!(2, map.height);
        assert_eq!(2, map.width);
    }
}
