use std::env;
use std::io::{self, BufRead};

fn to_dec<I>(binary_vec: I) -> i32
where
    I: DoubleEndedIterator<Item = i32>,
{
    return binary_vec
        .rev()
        .enumerate()
        .fold(0, |result, (i, bit)| result + bit * i32::pow(2, i as u32));
}

fn count_ones(lines: &Vec<String>) -> Vec<u32> {
    let mut one_amounts: Vec<u32> = vec![0; lines.first().expect("No lines read!").len()];
    for line in lines.iter() {
        for (i, val) in line.chars().enumerate() {
            one_amounts[i] += if val == '1' { 1 } else { 0 }
        }
    }
    one_amounts
}

fn problem1(lines: Vec<String>) {
    let one_amounts = count_ones(&lines);
    let gamma = to_dec(one_amounts.iter().map(|amount| {
        if *amount > lines.len() as u32 / 2 {
            1
        } else {
            0
        }
    }));
    let epsilon = to_dec(one_amounts.iter().map(|amount| {
        if *amount > lines.len() as u32 / 2 {
            0
        } else {
            1
        }
    }));
    println!("{} {} {}", lines.len(), gamma, epsilon);
    println!("{}", gamma * epsilon)
}

fn string_to_bool_vec(line: &String) -> Vec<bool> {
    line.chars().map(|c| c == '1').collect::<Vec<_>>()
}

fn to_bool_vec(lines: Vec<String>) -> Vec<Vec<bool>> {
    lines
        .iter()
        .map(|line| string_to_bool_vec(line))
        .collect::<Vec<_>>()
}

fn bit_checker(lines: &Vec<&Vec<bool>>, index: usize) -> bool {
    let num_ones = lines
        .iter()
        .fold(0, |acc, line| acc + if line[index] { 1 } else { 0 });
    num_ones >= lines.len() - num_ones
}

fn print_bool_vec(vec: &Vec<bool>) {
    for boolean in vec.iter() {
        print!("{}", if *boolean { '1' } else { '0' })
    }
    println!();
}

fn get_rating(lines: Vec<&Vec<bool>>, index: usize, oxigen: bool) -> Vec<bool> {
    if lines.len() == 1 {
        return lines[0].to_vec();
    }
    let filter_one = bit_checker(&lines, index);
    let filtered: Vec<&Vec<bool>> = lines
        .iter()
        .filter(|line| {
            if oxigen {
                line[index] == filter_one
            } else {
                line[index] != filter_one
            }
        })
        .map(|line| *line)
        .collect();
    return get_rating(filtered, index + 1, oxigen);
}

fn problem2(lines: Vec<String>) {
    let bool_lines = to_bool_vec(lines);

    let oxigen_rating = &get_rating(bool_lines.iter().collect(), 0, true);
    let co2_rating = &get_rating(bool_lines.iter().collect(), 0, false);
    print_bool_vec(oxigen_rating);
    print_bool_vec(co2_rating);
    let oxigen = to_dec(
        oxigen_rating
            .iter()
            .map(|boolean| if *boolean { 1 } else { 0 }),
    );
    let co2 = to_dec(
        co2_rating
            .iter()
            .map(|boolean| if *boolean { 1 } else { 0 }),
    );
    println!("{} {} {}", oxigen, co2, oxigen * co2)
}

fn main() {
    let lines = io::stdin()
        .lock()
        .lines()
        .map(|line| line.expect("Could not read line"))
        .collect::<Vec<_>>();

    let args: Vec<String> = env::args().collect();
    let mode = &args[1];
    match mode.as_str() {
        "1" => problem1(lines),
        "2" => problem2(lines),
        _ => println!("uhm que pasa?"),
    }
}
