use std::collections::HashMap;
pub enum Problem {
    First,
    Second,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Pair {
    first: char,
    second: char,
}

fn parse_insertion_rules(lines: impl Iterator<Item = String>) -> HashMap<Pair, (Pair, Pair)> {
    let mut rules = HashMap::new();
    for line in lines {
        let mut split_line = line.split(" -> ");
        let pair = split_line.next().expect("No first pair");
        let first = pair.chars().nth(0).expect("No first char");
        let second = pair.chars().nth(1).expect("No second char");
        let rule_char = split_line
            .next()
            .expect("No rule char")
            .chars()
            .nth(0)
            .expect("No rule char");
        rules.insert(
            Pair { first, second },
            (
                Pair {
                    first,
                    second: rule_char,
                },
                Pair {
                    first: rule_char,
                    second,
                },
            ),
        );
    }
    rules
}

fn get_all_pairs(chars: &Vec<char>) -> Vec<Pair> {
    let mut pairs = Vec::new();
    for first in chars.iter() {
        for second in chars.iter() {
            pairs.push(Pair {
                first: *first,
                second: *second,
            })
        }
    }
    pairs
}

fn extend_map(map: &mut HashMap<char, u64>, other: &HashMap<char, u64>) {
    for (key, value) in other.iter() {
        *map.entry(*key).or_insert(0) += value;
    }
}

fn generate_formula_map(
    chars: &Vec<char>,
    steps: u32,
    rules: &HashMap<Pair, (Pair, Pair)>,
) -> HashMap<Pair, HashMap<char, u64>> {
    let pairs = get_all_pairs(chars);
    let mut formula_map: HashMap<Pair, HashMap<char, u64>> = HashMap::new();
    for pair in pairs.iter() {
        *formula_map
            .entry(*pair)
            .or_insert(HashMap::new())
            .entry(pair.first)
            .or_insert(0) += 1;
        *formula_map
            .entry(*pair)
            .or_insert(HashMap::new())
            .entry(pair.first)
            .or_insert(0) += 1;
    }
    for _i in 0..steps {
        let mut next_formula_map: HashMap<Pair, HashMap<char, u64>> = HashMap::new();
        for pair in pairs.iter() {
            if rules.contains_key(pair) {
                let (first_child, second_child) = rules[pair];
                let mut char_count = formula_map[&first_child].clone();
                extend_map(&mut char_count, &formula_map[&second_child]);
                next_formula_map.insert(*pair, char_count);
            } else {
                next_formula_map.insert(*pair, formula_map[pair].clone());
            }
        }
        formula_map = next_formula_map;
    }
    formula_map
}

fn get_occourences(
    template: &String,
    formula_map: &HashMap<Pair, HashMap<char, u64>>,
) -> Vec<(char, u64)> {
    let mut template_pairs = Vec::new();
    for i in 0..template.len() - 1 {
        let first = template.chars().nth(i).expect("No char here");
        let second = template.chars().nth(i + 1).expect("No char here");
        template_pairs.push(Pair { first, second })
    }
    let mut char_count = HashMap::new();
    for pair in template_pairs {
        extend_map(&mut char_count, &formula_map[&pair])
    }
    println!("{:?}", char_count);
    char_count = HashMap::from_iter(char_count.iter().map(|(c, count)| (*c, count / 2)));
    *char_count
        .entry(
            template
                .chars()
                .nth(template.len() - 1)
                .expect("No chars in template"),
        )
        .or_insert(0) += 1;
    char_count.iter().map(|(c, count)| (*c, *count)).collect()
}

pub fn run(lines: &mut impl Iterator<Item = String>, mode: Problem) {
    let template = lines.next().expect("Could not read template");
    let rules = parse_insertion_rules(lines.skip(1));
    let steps = match mode {
        Problem::First => 10,
        Problem::Second => 40,
    };
    let formula_map = generate_formula_map(
        &"ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().collect(),
        steps,
        &rules,
    );
    let occourences = get_occourences(&template, &formula_map);
    println!("{:?}", occourences);
    let max = occourences
        .iter()
        .map(|(_c, count)| count)
        .max()
        .expect("No max?!");
    let min = occourences
        .iter()
        .map(|(_c, count)| count)
        .min()
        .expect("No min!?");
    println!("{}", max - min)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_parse() {
        //Given
        let lines = vec!["CH -> B", "CB -> H"];
        //When
        let rules = parse_insertion_rules(lines.iter().map(|line| String::from(*line)));
        //Then
        assert_eq!(2, rules.len());
        assert_eq!(
            (
                Pair {
                    first: 'C',
                    second: 'B'
                },
                Pair {
                    first: 'B',
                    second: 'H'
                }
            ),
            *rules
                .get(&Pair {
                    first: 'C',
                    second: 'H'
                })
                .unwrap()
        )
    }

    #[test]
    fn test_generate_formula_map() {
        //Given
        let chars = vec!['A'];
        let steps = 1;
        let rules: HashMap<Pair, (Pair, Pair)> = HashMap::new();
        //When
        let map: HashMap<Pair, HashMap<char, u64>> = generate_formula_map(&chars, steps, &rules);
        //Then
        assert_eq!(
            2,
            map[&Pair {
                first: 'A',
                second: 'A'
            }][&'A']
        );
    }

    #[test]
    fn test_generate_formula_map_with_rules() {
        //Given
        let chars = vec!['A'];
        let steps = 1;
        let rules: HashMap<Pair, (Pair, Pair)> =
            parse_insertion_rules(vec!["AA -> A"].iter().map(|line| String::from(*line)));
        //When
        let map: HashMap<Pair, HashMap<char, u64>> = generate_formula_map(&chars, steps, &rules);
        //Then
        assert_eq!(
            4,
            map[&Pair {
                first: 'A',
                second: 'A'
            }][&'A']
        );
    }

    #[test]
    fn test_count_occourence() {
        //GIVEN
        let template = String::from("AA");
        let formula_map = HashMap::from([(
            Pair {
                first: 'A',
                second: 'A',
            },
            HashMap::from([('A', 4)]),
        )]);
        //WHEN
        let occourences: Vec<(char, u64)> = get_occourences(&template, &formula_map);
        //THEN
        assert_eq!(vec![('A', 3)], occourences);
    }
}
