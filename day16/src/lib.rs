use std::collections::HashMap;

pub enum Problem {
    First,
    Second,
}

#[derive(Debug)]
struct Header {
    version: u8,
    packet_type: u8,
}

trait Packet {
    fn header(&self) -> &Header;
    fn version_sum(&self) -> u32;
    fn calc_value(&self) -> u64;
}

#[derive(Debug)]
struct Literal {
    header: Header,
    value: u64,
}

impl Packet for Literal {
    fn header(&self) -> &Header {
        &self.header
    }
    fn version_sum(&self) -> u32 {
        self.header().version as u32
    }

    fn calc_value(&self) -> u64 {
        self.value
    }
}

struct Operator {
    header: Header,
    sub_packages: Vec<Box<dyn Packet>>,
}

impl Packet for Operator {
    fn header(&self) -> &Header {
        &self.header
    }
    fn version_sum(&self) -> u32 {
        self.sub_packages
            .iter()
            .map(|packet| packet.version_sum())
            .sum::<u32>()
            + self.header.version as u32
    }

    fn calc_value(&self) -> u64 {
        let mut sub_values = self.sub_packages.iter().map(|packet| packet.calc_value());
        match self.header.packet_type {
            0 => sub_values.sum(),
            1 => sub_values.fold(1, |acc, curr| acc * curr),
            2 => sub_values.min().expect("No min!"),
            3 => sub_values.max().expect("No max!"),
            5 => {
                let first = sub_values.next().expect("No first in gt");
                let second = sub_values.next().expect("No second in gt");
                if first > second {
                    1
                } else {
                    0
                }
            }
            6 => {
                let first = sub_values.next().expect("No first in lt");
                let second = sub_values.next().expect("No second in lt");
                if first < second {
                    1
                } else {
                    0
                }
            }
            7 => {
                let first = sub_values.next().expect("No first in eq");
                let second = sub_values.next().expect("No second in eq");
                if first == second {
                    1
                } else {
                    0
                }
            }
            _ => panic!("Unknown packet type!"),
        }
    }
}

fn to_bin(line: &String) -> String {
    let hex_map = HashMap::from([
        ('0', "0000"),
        ('1', "0001"),
        ('2', "0010"),
        ('3', "0011"),
        ('4', "0100"),
        ('5', "0101"),
        ('6', "0110"),
        ('7', "0111"),
        ('8', "1000"),
        ('9', "1001"),
        ('A', "1010"),
        ('B', "1011"),
        ('C', "1100"),
        ('D', "1101"),
        ('E', "1110"),
        ('F', "1111"),
    ]);
    line.chars().flat_map(|c| hex_map[&c].chars()).collect()
}

//msb first
fn parse_bin(bin_iter: impl Iterator<Item = char>) -> Result<u64, std::num::ParseIntError> {
    let string = String::from_iter(bin_iter);
    u64::from_str_radix(&string, 2)
}

fn read_header(bin_iter: &mut impl Iterator<Item = char>) -> Option<Header> {
    Some(Header {
        version: parse_bin(bin_iter.take(3)).ok()? as u8,
        packet_type: parse_bin(bin_iter.take(3)).ok()? as u8,
    })
}

fn read_literal(
    bin_iter: &mut impl Iterator<Item = char>,
    header: Header,
) -> Option<Box<dyn Packet>> {
    let mut nibbles: String = String::new();
    let mut current_nibble: String = bin_iter.take(5).collect();
    while current_nibble.chars().nth(0)? == '1' {
        nibbles.extend(current_nibble.chars().skip(1));
        current_nibble = bin_iter.take(5).collect();
    }
    nibbles.extend(current_nibble.chars().skip(1));
    let value = parse_bin(nibbles.chars()).ok()?;
    Some(Box::new(Literal { header, value }))
}

fn read_sub_packages_num(
    bin_iter: &mut impl Iterator<Item = char>,
    num_packets: usize,
) -> Vec<Box<dyn Packet>> {
    (0..num_packets)
        .map(|_i| read_packet(bin_iter).expect("Could not read sub package!"))
        .collect()
}

fn read_sub_packages_fixed_size(bits: String) -> Vec<Box<dyn Packet>> {
    let mut packets = vec![];
    let mut packet_iter = bits.chars();
    loop {
        match read_packet(&mut packet_iter) {
            Some(packet) => packets.push(packet),
            None => break,
        }
    }
    packets
}

fn read_operator(
    bin_iter: &mut impl Iterator<Item = char>,
    header: Header,
) -> Option<Box<dyn Packet>> {
    let length_type = bin_iter.next()?;
    let sub_packages = match length_type {
        '1' => {
            let num_sum_packets = parse_bin(bin_iter.take(11)).ok()?;
            read_sub_packages_num(bin_iter, num_sum_packets as usize)
        }
        '0' => {
            let num_bits = parse_bin(bin_iter.take(15)).ok()? as usize;
            read_sub_packages_fixed_size(bin_iter.take(num_bits).collect())
        }
        _ => return None,
    };
    Some(Box::new(Operator {
        header,
        sub_packages,
    }))
}

fn read_packet(bin_iter: &mut impl Iterator<Item = char>) -> Option<Box<dyn Packet>> {
    let header = read_header(bin_iter)?;
    match header.packet_type {
        4 => read_literal(bin_iter, header),
        _ => read_operator(bin_iter, header),
    }
}

pub fn run(lines: impl Iterator<Item = String>, mode: Problem) {
    for line in lines {
        let bin_str = to_bin(&line);
        let mut bin_iter = bin_str.chars();
        let packet: Box<dyn Packet> = read_packet(&mut bin_iter).expect("Could not read packet!");
        match mode {
            Problem::First => println!("version_sum: {}", packet.version_sum()),
            Problem::Second => println!("Result: {}", packet.calc_value()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn convert_hexa() {
        //GIVEN
        let input = "F1";
        //WHEN
        let bin_iter = to_bin(&String::from(input));
        //THEN
        assert_eq!("11110001", bin_iter)
    }

    #[test]
    fn test_read_header() {
        //GIVEN
        let bin_str = to_bin(&String::from("D2FE28"));
        let mut bin_iter = bin_str.chars();
        //WHEN

        let header = read_header(&mut bin_iter).expect("Could not read header");
        //THEN
        assert_eq!(6, header.version);
        assert_eq!(4, header.packet_type);
        assert_eq!(18, bin_iter.count())
    }

    #[test]
    fn test_read_literal() {
        //GIVEN
        //001_100_00001_000 = 0011_0000_0010 = 302
        let bin_str = to_bin(&String::from("302"));
        let mut bin_iter = bin_str.chars();
        //WHEN
        let literal = read_packet(&mut bin_iter).expect("Could not read packet");
        //THEN
        assert_eq!(1, bin_iter.count());
        assert_eq!(1, literal.calc_value())
    }

    #[test]
    fn test_read_operator_lt1() {
        //GIVEN
        //001_000_1_00000000001_010_100_00001 = 0010_0010_0000_0000_0101_0100_0000_1000 = 22005408
        //{v:1,t:0,l:1:1,{v:2,t:4,val:1}}
        let bin_str = to_bin(&String::from("22005408"));
        let mut bin_iter = bin_str.chars();
        //WHEN
        let operator = read_packet(&mut bin_iter).expect("Could not read packet!");
        //THEN
        assert_eq!(3, bin_iter.count(), "Read unexpected amount of bits");
        assert_eq!(3, operator.version_sum(), "Version sum was not correct");
        assert_eq!(1, operator.calc_value())
    }

    #[test]
    fn test_read_operator_lt0() {
        //GIVEN
        //001_110_0_000000000011011_110_100_01010_010_100_1000100100_0000000
        //{v:1, t:6, l:0:27, {v:6,t:4,val: 10}{v:2, t:4, val: 20}}
        let bin_str = to_bin(&String::from("38006F45291200"));
        let mut bin_iter = bin_str.chars();
        //WHEN
        let operator = read_packet(&mut bin_iter).expect("Could not read packet!");
        //THEN
        assert_eq!(7, bin_iter.count(), "Read unexpected amount of bits");
        assert_eq!(
            1 + 6 + 2,
            operator.version_sum(),
            "Version sum was not correct"
        );
        assert_eq!(1, operator.calc_value())
    }

    #[test]
    fn test_rec() {
        //GIVEN
        /*
        //011_000_1_00000000010_
        //  000_000_0_000000000010110_
        //      000_100_01010_
        //      101_100_01011_
        //  001_000_1_00000000010_
        //      000_100_01100_
        //      011_100_01101_00
        //{
        //  ver: 3
        //  type: 0
        //  length: 2 subpackages
        //  {
        //      ver: 0
        //      type: 0
        //      length: 22bits
        //      {
        //          {
        //          ver: 0
        //          type: 4
        //          value: 10
        //          }
        //          {
                    ver: 5
                    type: 4
                    value: 11
                    }
        //      }
        //  }
            {
                ver: 1
                type: 0
                length: 2 packages
                {
                    {
                    ver: 0
                    value: 12
                    }
                    {
                    ver: 3
                    value: 13
                    }
                }
            }
        //}
        så: 10+11+12+13 = 46
        */
        let bin_str = to_bin(&String::from("620080001611562C8802118E34"));
        let mut bin_iter = bin_str.chars();
        //WHEN
        let operator = read_packet(&mut bin_iter).expect("Could not read packet!");
        //THEN
        assert_eq!(2, bin_iter.count(), "Read unexpected amount of bits");
        assert_eq!(12, operator.version_sum(), "Version sum was not correct");
        assert_eq!(46, operator.calc_value())
    }
}
